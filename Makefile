REPOSITORY ?= sebdoido/mower
BUILD_ID   ?= 1
VERSION    ?= $(shell git describe --abbrev=1 --tags --always)
IMAGE      ?= $(REPOSITORY):$(VERSION)-$(BUILD_ID)

CHECK_FILES ?=$$(go list ./... | grep -v /vendor/)

default: app

app: | lint vet run-tests ## Build the app
	CGO_ENABLED=0 go build -a -ldflags "-extldflags '-static' -X 'main.buildTime=$(shell date -u '+%Y-%m-%d %H:%M:%S')' -X main.version=$(VERSION)" -o app .
	./app

build: lint vet run-tests ## Build a docker image
	docker build --pull --no-cache --build-arg APP_VERSION=$(VERSION) -t $(IMAGE) .

run-tests: ## Run tests.
	@go test -p 1 -v ./...

vet: # Vet the code
	@go vet $(CHECK_FILES)
	
lint: ## Lint the code.
	@golint $(CHECK_FILES)

run: ## Run the docker image
	docker run -v $(PWD)/data:/data $(IMAGE) -file /data

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: app build lint vet run-tests help
