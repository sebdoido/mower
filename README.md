# Description
Running blablatech tests :-)

# Usage
## Run tests and app locally
```
make app
```

## Build & run docker image
```
make build run
```

## Example
```
➜  mower git:(master) make app
=== RUN   TestMowerValidateOrientation
=== PAUSE TestMowerValidateOrientation
=== RUN   TestMowerValidateMovement
=== PAUSE TestMowerValidateMovement
=== RUN   TestMowerRotation
=== PAUSE TestMowerRotation
=== RUN   TestMowerActions
=== PAUSE TestMowerActions
=== RUN   TestMowerParseInputData
=== PAUSE TestMowerParseInputData
=== CONT  TestMowerValidateOrientation
=== RUN   TestMowerValidateOrientation/#00
=== CONT  TestMowerActions
=== RUN   TestMowerValidateOrientation/#01
=== RUN   TestMowerActions/#00
=== RUN   TestMowerValidateOrientation/#02
=== RUN   TestMowerValidateOrientation/#03
=== RUN   TestMowerValidateOrientation/#04
=== RUN   TestMowerActions/#01
=== RUN   TestMowerValidateOrientation/#05
=== RUN   TestMowerValidateOrientation/#06
=== RUN   TestMowerValidateOrientation/#07
--- PASS: TestMowerValidateOrientation (0.00s)
    --- PASS: TestMowerValidateOrientation/#00 (0.00s)
    --- PASS: TestMowerValidateOrientation/#01 (0.00s)
    --- PASS: TestMowerValidateOrientation/#02 (0.00s)
    --- PASS: TestMowerValidateOrientation/#03 (0.00s)
    --- PASS: TestMowerValidateOrientation/#04 (0.00s)
    --- PASS: TestMowerValidateOrientation/#05 (0.00s)
    --- PASS: TestMowerValidateOrientation/#06 (0.00s)
    --- PASS: TestMowerValidateOrientation/#07 (0.00s)
=== CONT  TestMowerRotation
=== RUN   TestMowerActions/#02
=== RUN   TestMowerRotation/#00
=== RUN   TestMowerRotation/#01
=== RUN   TestMowerRotation/#02
=== RUN   TestMowerRotation/#03
=== RUN   TestMowerRotation/#04
=== RUN   TestMowerActions/#03
=== RUN   TestMowerRotation/#05
=== RUN   TestMowerRotation/#06
=== RUN   TestMowerRotation/#07
--- PASS: TestMowerActions (0.00s)
    --- PASS: TestMowerActions/#00 (0.00s)
    --- PASS: TestMowerActions/#01 (0.00s)
    --- PASS: TestMowerActions/#02 (0.00s)
    --- PASS: TestMowerActions/#03 (0.00s)
=== CONT  TestMowerValidateMovement
=== RUN   TestMowerValidateMovement/#00
=== RUN   TestMowerRotation/#08
=== RUN   TestMowerValidateMovement/#01
=== RUN   TestMowerRotation/#09
=== RUN   TestMowerValidateMovement/#02
=== RUN   TestMowerValidateMovement/#03
=== RUN   TestMowerValidateMovement/#04
=== RUN   TestMowerRotation/#10
=== RUN   TestMowerValidateMovement/#05
=== RUN   TestMowerRotation/#11
=== RUN   TestMowerValidateMovement/#06
--- PASS: TestMowerValidateMovement (0.00s)
    --- PASS: TestMowerValidateMovement/#00 (0.00s)
    --- PASS: TestMowerValidateMovement/#01 (0.00s)
    --- PASS: TestMowerValidateMovement/#02 (0.00s)
    --- PASS: TestMowerValidateMovement/#03 (0.00s)
    --- PASS: TestMowerValidateMovement/#04 (0.00s)
    --- PASS: TestMowerValidateMovement/#05 (0.00s)
    --- PASS: TestMowerValidateMovement/#06 (0.00s)
=== CONT  TestMowerParseInputData
=== RUN   TestMowerParseInputData/#00
--- PASS: TestMowerRotation (0.00s)
    --- PASS: TestMowerRotation/#00 (0.00s)
    --- PASS: TestMowerRotation/#01 (0.00s)
    --- PASS: TestMowerRotation/#02 (0.00s)
    --- PASS: TestMowerRotation/#03 (0.00s)
    --- PASS: TestMowerRotation/#04 (0.00s)
    --- PASS: TestMowerRotation/#05 (0.00s)
    --- PASS: TestMowerRotation/#06 (0.00s)
    --- PASS: TestMowerRotation/#07 (0.00s)
    --- PASS: TestMowerRotation/#08 (0.00s)
    --- PASS: TestMowerRotation/#09 (0.00s)
    --- PASS: TestMowerRotation/#10 (0.00s)
    --- PASS: TestMowerRotation/#11 (0.00s)
=== RUN   TestMowerParseInputData/#01
--- PASS: TestMowerParseInputData (0.00s)
    --- PASS: TestMowerParseInputData/#00 (0.00s)
    --- PASS: TestMowerParseInputData/#01 (0.00s)
PASS
ok  	gitlab.com/sebdoido/mower	0.004s
CGO_ENABLED=0 go build -a -ldflags "-extldflags '-static' -X 'main.buildTime=2019-06-17 09:50:39' -X main.version=4fc2" -o app .
./app
INFO[0000] Starting mower app.. version=4fc2, date=2019-06-17 09:50:39 
INFO[0000] Starting new surface                          surface="Surface [5 ; 5]"
INFO[0000] Starting mower                                action=LFLFLFLFF mower="currently in position [1 ; 2] with [N] orientation"
INFO[0000] Ending mower                                  action=LFLFLFLFF mower="currently in position [1 ; 3] with [N] orientation"
INFO[0000] Starting mower                                action=FFRFFRFRRF mower="currently in position [3 ; 3] with [E] orientation"
INFO[0000] Ending mower                                  action=FFRFFRFRRF mower="currently in position [5 ; 1] with [E] orientation"
```

## Run with verbose logs
```
./app -file data -debug
➜  mower git:(master) ✗ ./app -file data -debug 
INFO[0000] Starting mower app.. version=4fc2, date=2019-06-17 09:50:39 
INFO[0000] Starting new surface                          surface="Surface [5 ; 5]"
INFO[0000] Starting mower                                action=LFLFLFLFF mower="currently in position [1 ; 2] with [N] orientation"
DEBU[0000] performing action                             X=1 Y=2 action=L orientation=90
DEBU[0000] now in position...                            X=1 Y=2 orientation=0
DEBU[0000] performing action                             X=1 Y=2 action=F orientation=0
DEBU[0000] moving forward...                             vertex="-1;0"
DEBU[0000] now in position...                            X=0 Y=2 orientation=0
DEBU[0000] performing action                             X=0 Y=2 action=L orientation=0
DEBU[0000] now in position...                            X=0 Y=2 orientation=270
DEBU[0000] performing action                             X=0 Y=2 action=F orientation=270
DEBU[0000] moving forward...                             vertex="0;-1"
DEBU[0000] now in position...                            X=0 Y=1 orientation=270
DEBU[0000] performing action                             X=0 Y=1 action=L orientation=270
DEBU[0000] now in position...                            X=0 Y=1 orientation=180
DEBU[0000] performing action                             X=0 Y=1 action=F orientation=180
DEBU[0000] moving forward...                             vertex="1;0"
DEBU[0000] now in position...                            X=1 Y=1 orientation=180
DEBU[0000] performing action                             X=1 Y=1 action=L orientation=180
DEBU[0000] now in position...                            X=1 Y=1 orientation=90
DEBU[0000] performing action                             X=1 Y=1 action=F orientation=90
DEBU[0000] moving forward...                             vertex="0;1"
DEBU[0000] now in position...                            X=1 Y=2 orientation=90
DEBU[0000] performing action                             X=1 Y=2 action=F orientation=90
DEBU[0000] moving forward...                             vertex="0;1"
DEBU[0000] now in position...                            X=1 Y=3 orientation=90
INFO[0000] Ending mower                                  action=LFLFLFLFF mower="currently in position [1 ; 3] with [N] orientation"
INFO[0000] Starting mower                                action=FFRFFRFRRF mower="currently in position [3 ; 3] with [E] orientation"
DEBU[0000] performing action                             X=3 Y=3 action=F orientation=180
DEBU[0000] moving forward...                             vertex="1;0"
DEBU[0000] now in position...                            X=4 Y=3 orientation=180
DEBU[0000] performing action                             X=4 Y=3 action=F orientation=180
DEBU[0000] moving forward...                             vertex="1;0"
DEBU[0000] now in position...                            X=5 Y=3 orientation=180
DEBU[0000] performing action                             X=5 Y=3 action=R orientation=180
DEBU[0000] now in position...                            X=5 Y=3 orientation=270
DEBU[0000] performing action                             X=5 Y=3 action=F orientation=270
DEBU[0000] moving forward...                             vertex="0;-1"
DEBU[0000] now in position...                            X=5 Y=2 orientation=270
DEBU[0000] performing action                             X=5 Y=2 action=F orientation=270
DEBU[0000] moving forward...                             vertex="0;-1"
DEBU[0000] now in position...                            X=5 Y=1 orientation=270
DEBU[0000] performing action                             X=5 Y=1 action=R orientation=270
DEBU[0000] now in position...                            X=5 Y=1 orientation=0
DEBU[0000] performing action                             X=5 Y=1 action=F orientation=0
DEBU[0000] moving forward...                             vertex="-1;0"
DEBU[0000] now in position...                            X=4 Y=1 orientation=0
DEBU[0000] performing action                             X=4 Y=1 action=R orientation=0
DEBU[0000] now in position...                            X=4 Y=1 orientation=90
DEBU[0000] performing action                             X=4 Y=1 action=R orientation=90
DEBU[0000] now in position...                            X=4 Y=1 orientation=180
DEBU[0000] performing action                             X=4 Y=1 action=F orientation=180
DEBU[0000] moving forward...                             vertex="1;0"
DEBU[0000] now in position...                            X=5 Y=1 orientation=180
INFO[0000] Ending mower                                  action=FFRFFRFRRF mower="currently in position [5 ; 1] with [E] orientation"

```
