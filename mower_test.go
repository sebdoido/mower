package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestMowerValidateOrientation(t *testing.T) {
	t.Parallel()

	data := []struct {
		orientation string
		expected    bool
	}{
		{expected: true, orientation: "N"},
		{expected: true, orientation: "S"},
		{expected: true, orientation: "E"},
		{expected: true, orientation: "W"},
		{expected: false, orientation: "A"},
		{expected: false, orientation: "NSEW"},
		{expected: false, orientation: ""},
		{expected: false, orientation: "1"},
	}

	for _, d := range data {
		t.Run("", func(t *testing.T) {
			res := validateOrientation(d.orientation)
			if res != d.expected {
				t.Errorf("Test failed. Expected '%v'. Actual '%v'. Input: '%v'", d.expected, res, d.orientation)
			}
		})
	}
}

func TestMowerValidateMovement(t *testing.T) {
	t.Parallel()

	data := []struct {
		movement string
		expected bool
	}{
		{expected: true, movement: "L"},
		{expected: true, movement: "R"},
		{expected: true, movement: "F"},
		{expected: false, movement: "A"},
		{expected: false, movement: "LRF"},
		{expected: false, movement: ""},
		{expected: false, movement: "1"},
	}

	for _, d := range data {
		t.Run("", func(t *testing.T) {
			res := validateMovement(d.movement)
			if res != d.expected {
				t.Errorf("Test failed. Expected '%v'. Actual '%v'. Input: '%v'", d.expected, res, d.movement)
			}
		})
	}
}

func TestMowerRotation(t *testing.T) {
	t.Parallel()

	surface := &Surface{1, 1}

	data := []struct {
		mower    Mower
		expected string
	}{
		{expected: "N", mower: Mower{X: 1, Y: 1, Orientation: Orientation["E"], Actions: "L"}},
		{expected: "S", mower: Mower{X: 1, Y: 1, Orientation: Orientation["W"], Actions: "L"}},
		{expected: "E", mower: Mower{X: 1, Y: 1, Orientation: Orientation["S"], Actions: "L"}},
		{expected: "W", mower: Mower{X: 1, Y: 1, Orientation: Orientation["N"], Actions: "L"}},
		{expected: "N", mower: Mower{X: 1, Y: 1, Orientation: Orientation["N"], Actions: "LLLL"}},
		{expected: "N", mower: Mower{X: 1, Y: 1, Orientation: Orientation["W"], Actions: "R"}},
		{expected: "S", mower: Mower{X: 1, Y: 1, Orientation: Orientation["E"], Actions: "R"}},
		{expected: "E", mower: Mower{X: 1, Y: 1, Orientation: Orientation["N"], Actions: "R"}},
		{expected: "W", mower: Mower{X: 1, Y: 1, Orientation: Orientation["S"], Actions: "R"}},
		{expected: "N", mower: Mower{X: 1, Y: 1, Orientation: Orientation["N"], Actions: "RRRR"}},
		{expected: "N", mower: Mower{X: 1, Y: 1, Orientation: Orientation["N"], Actions: "LR"}},
		{expected: "S", mower: Mower{X: 1, Y: 1, Orientation: Orientation["S"], Actions: "LRLR"}},
	}

	for _, d := range data {
		t.Run("", func(t *testing.T) {
			d.mower.Perform(surface)
			o, _ := Orientation.key(d.mower.Orientation)
			if d.expected != o {
				t.Errorf("Test failed. Expected '%v'. Actual '%v'. Mower: '%v'", d.expected, o, d.mower)
			}
		})
	}
}

func TestMowerActions(t *testing.T) {
	t.Parallel()

	surface := &Surface{5, 5}

	data := []struct {
		mower    Mower
		expected result
	}{
		{expected: result{1, 2, "N"}, mower: Mower{X: 1, Y: 1, Orientation: Orientation["N"], Actions: "F"}},
		{expected: result{0, 0, "W"}, mower: Mower{X: 0, Y: 0, Orientation: Orientation["N"], Actions: "LF"}}, // border detection
		{expected: result{1, 3, "N"}, mower: Mower{X: 1, Y: 2, Orientation: Orientation["N"], Actions: "LFLFLFLFF"}},
		{expected: result{5, 1, "E"}, mower: Mower{X: 3, Y: 3, Orientation: Orientation["E"], Actions: "FFRFFRFRRF"}},
	}

	for _, d := range data {
		t.Run("", func(t *testing.T) {
			d.mower.Perform(surface)
			o, _ := Orientation.key(d.mower.Orientation)
			if d.expected.orientation != o || d.expected.X != d.mower.X || d.mower.Y != d.expected.Y {
				t.Errorf("Test failed. Expected '%v'. Actual '%v'. Mower: '%v'", d.expected, o, d.mower)
			}
		})
	}
}

func TestMowerParseInputData(t *testing.T) {
	t.Parallel()

	data := []struct {
		input           string
		expectedSurface Surface
		expectedMowers  Mowers
	}{
		{
			input:           "5 5\n1 2 N\nLFLFLFLFF",
			expectedSurface: Surface{5, 5},
			expectedMowers: []*Mower{
				&Mower{X: 1, Y: 2, Orientation: Orientation["N"], Actions: "LFLFLFLFF"},
			},
		},
		{
			input:           "5 5\n1 2 N\nLFLFLFLFF\n3 3 E\nFFRFFRFRRF",
			expectedSurface: Surface{5, 5},
			expectedMowers: []*Mower{
				&Mower{X: 1, Y: 2, Orientation: Orientation["N"], Actions: "LFLFLFLFF"},
				&Mower{X: 3, Y: 3, Orientation: Orientation["E"], Actions: "FFRFFRFRRF"},
			},
		},
	}

	for _, d := range data {
		t.Run("", func(t *testing.T) {
			resSurface, resMower, _ := parseDataContent(strings.NewReader(d.input))

			if d.expectedSurface.Width != resSurface.Width || d.expectedSurface.Height != resSurface.Height {
				t.Errorf("Test failed. Expected Surface '%v'. Actual '%v'", d.expectedSurface, resSurface)
			}

			if !resMower.Compare(d.expectedMowers) {
				t.Errorf("Test failed. Expected mower '%v'. Actual '%v'", d.expectedMowers, resMower)
			}
		})
	}
}

type result struct {
	X, Y        int64
	orientation string
}

func (r result) String() string {
	return fmt.Sprintf("X: %v ; Y: %v ; Orientation: %v", r.X, r.Y, r.orientation)
}
