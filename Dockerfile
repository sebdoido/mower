FROM golang:1.12 as builder
ARG APP_VERSION
WORKDIR /code
ADD go.mod go.sum /code/
RUN go mod download
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags "-extldflags '-static' -X 'main.buildTime=$(date -u '+%Y-%m-%d %H:%M:%S')' -X main.version=${APP_VERSION}" -o /app .

FROM alpine:3.9
RUN apk --update upgrade && apk add --no-cache ca-certificates
RUN addgroup -g 1000 -S mower && adduser -u 1000 -S mower -G mower
USER mower
COPY --from=builder /app /app
ENTRYPOINT ["/app"]
