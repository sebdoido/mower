package main

import "fmt"

// OrientationDegree will be used to get an orientation degree regarding well-known orientation key (N, S, E, W)
type OrientationDegree map[string]int64

var (
	// Orientation ...
	Orientation = OrientationDegree{"W": 0, "N": 90, "E": 180, "S": 270}
)

func (o OrientationDegree) key(value int64) (key string, ok bool) {
	for k, v := range o {
		if v == value {
			key = k
			ok = true
			return
		}
	}
	return
}

// PrepareMovementStatement will return a Vertex X, Y coordinate to handle expected movement regarding current Orientation
func (o OrientationDegree) PrepareMovementStatement(currentOrientation int64) (*Vertex, error) {
	key, ok := o.key(currentOrientation)
	if !ok {
		return nil, fmt.Errorf("Orientation: unable to find key regarding current degree orientation")
	}
	if key == "W" {
		return &Vertex{-1, 0}, nil
	}
	if key == "E" {
		return &Vertex{1, 0}, nil
	}
	if key == "N" {
		return &Vertex{0, 1}, nil
	}
	if key == "S" {
		return &Vertex{0, -1}, nil
	}
	return nil, nil
}

// Vertex describes an expected movement
type Vertex struct {
	X, Y int64
}

func (v Vertex) String() string {
	return fmt.Sprintf("%d;%d", v.X, v.Y)
}
