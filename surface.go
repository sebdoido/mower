package main

import (
	"fmt"
	"strconv"
	"strings"
)

// Surface is described by a width and a height
type Surface struct {
	Width, Height int64
}

// NewSurface ...
func NewSurface(data string) (*Surface, error) {
	tmp := strings.Fields(data)
	if len(tmp) != 2 {
		return nil, fmt.Errorf("Surface unknown args")
	}

	surface := Surface{}

	// Width
	i, err := strconv.ParseInt(tmp[0], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("surface: width parsing issue")
	}
	surface.Width = i

	// Height
	i, err = strconv.ParseInt(tmp[1], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("surface: width parsing issue")
	}
	surface.Height = i
	return &surface, nil
}

// String ...
func (s Surface) String() string {
	return fmt.Sprintf("Surface [%d ; %d]", s.Width, s.Height)
}
