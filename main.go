package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"

	log "github.com/sirupsen/logrus"
)

var (
	version   string
	buildTime string
)

func main() {
	input := flag.String("file", "data", "Input data file")
	debugMode := flag.Bool("debug", false, "Run in debug mode")
	flag.Parse()

	if *debugMode {
		log.SetLevel(log.DebugLevel)
	}
	log.Printf("Starting mower app.. version=%s, date=%s\n", version, buildTime)

	surface, mowers, err := loadInputFile(*input)
	if err != nil {
		log.WithField("error", err).Fatal("Unable to parse input file")
	}
	log.WithField("surface", surface).Infof("Starting new surface")

	for _, mower := range mowers {
		log.WithFields(log.Fields{"mower": mower, "action": mower.Actions}).Infof("Starting mower")
		mower.Perform(surface)
		log.WithFields(log.Fields{"mower": mower, "action": mower.Actions}).Infof("Ending mower")
	}
}

// load input data file to get surface and mowers
func loadInputFile(path string) (*Surface, Mowers, error) {
	inFile, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}
	defer func() error {
		if err = inFile.Close(); err != nil {
			return err
		}
		return nil
	}()
	return parseDataContent(inFile)
}

// parse input content to get surface and mowers
func parseDataContent(inFile io.Reader) (*Surface, Mowers, error) {
	scanner := bufio.NewScanner(inFile)
	scanner.Split(bufio.ScanLines)

	var surface *Surface
	var mowers Mowers
	var err error

	// Handling first line to get Surface requirements
	if scanner.Scan() {
		line := scanner.Text()
		if surface, err = NewSurface(line); err != nil {
			return nil, nil, err
		}
	}

	// Loading mowers dataset
	startPositions, actions := "", ""
	i := 0
	for scanner.Scan() {
		line := scanner.Text()
		if i%2 == 0 {
			startPositions = line
		} else {
			actions = line
			mower, err := NewMower(startPositions, actions)
			if err != nil {
				return nil, nil, err
			}
			mowers = append(mowers, mower)
		}
		i++
	}
	if i%2 != 0 {
		return nil, nil, fmt.Errorf("each mower should have 2 lines to init positions and set actions")
	}
	err = scanner.Err()
	if err != nil {
		log.Fatal(err)
	}
	return surface, mowers, nil
}
