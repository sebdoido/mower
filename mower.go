package main

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

// Mower has X,Y coordinate
// an Orientation (0 for West orientation, 90 for North, 180 for East, 270 for south)
// and Actions to be performed
type Mower struct {
	X, Y        int64
	Orientation int64
	Actions     string
}

// NewMower create a Mower from startPosition and actions (string)
func NewMower(startPosition, actions string) (*Mower, error) {
	tmp := strings.Fields(startPosition)
	if len(tmp) != 3 {
		return nil, fmt.Errorf("Mower: unable to create a mower with data %s", startPosition)
	}

	mower := Mower{}

	// X
	i, err := strconv.ParseInt(tmp[0], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("mower: starting position parsing issue => unable to set 'x' coordinate")
	}
	mower.X = i

	// Y
	i, err = strconv.ParseInt(tmp[1], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("mower: starting position parsing issue => unable to set 'y' coordinate")
	}
	mower.Y = i

	// Orientation
	if validateOrientation(tmp[2]) {
		mower.Orientation = Orientation[tmp[2]]
	} else {
		return nil, fmt.Errorf("mower: unkown orientation")
	}

	// Actions
	mower.Actions = actions

	return &mower, nil
}

// Perform expected actions on the mower
func (m *Mower) Perform(surface *Surface) error {
	for _, action := range m.Actions {
		log.WithFields(log.Fields{"action": string(action), "X": m.X, "Y": m.Y, "orientation": m.Orientation}).Debugf("performing action")

		if !validateMovement(string(action)) {
			return fmt.Errorf("mower: unkown movement %c", action)
		}

		switch action {
		case 'F':
			m.move(surface)
		case 'L':
			m.rotate(action)
		case 'R':
			m.rotate(action)
		default:
			return fmt.Errorf("mower: unkown movement %c", action)
		}
		log.WithFields(log.Fields{"X": m.X, "Y": m.Y, "orientation": m.Orientation}).Debugf("now in position...")
	}
	return nil
}

// rotate the mower (left or right)
func (m *Mower) rotate(action rune) {
	if action == 'L' {
		m.Orientation -= 90
		if m.Orientation < 0 {
			m.Orientation = 270
		}
	}
	if action == 'R' {
		m.Orientation += 90
		if m.Orientation > 270 {
			m.Orientation = 0
		}
	}
}

// move forward the  mower regarding current orientation
func (m *Mower) move(surface *Surface) error {
	vertex, err := Orientation.PrepareMovementStatement(m.Orientation)
	log.WithField("vertex", vertex).Debugf("moving forward...")
	if err != nil {
		return fmt.Errorf("unable to prepare movement statement")
	}

	if m.X+vertex.X >= 0 && m.X+vertex.X <= surface.Width {
		m.X += vertex.X
	}

	if m.Y+vertex.Y >= 0 && m.Y+vertex.Y <= surface.Height {
		m.Y += vertex.Y
	}

	return nil
}

// check if orientation is valid
func validateOrientation(data string) bool {
	var validator = regexp.MustCompile(`^(N|S|E|W)$`)
	return validator.MatchString(data)
}

// check if movement is valid
func validateMovement(data string) bool {
	var validator = regexp.MustCompile(`^(L|R|F)$`)
	return validator.MatchString(data)
}

// String ...
func (m Mower) String() string {
	o, _ := Orientation.key(m.Orientation)
	return fmt.Sprintf("currently in position [%d ; %d] with [%v] orientation", m.X, m.Y, o)
}

// Mowers is a collection of mower that will operate on a surface
type Mowers []*Mower

// Compare two mowers collection
func (a Mowers) Compare(b Mowers) (_ bool) {
	if len(a) != len(b) {
		return
	}
	for i := range a {
		if !reflect.DeepEqual(a[i], b[i]) {
			return false
		}
	}

	return true
}
